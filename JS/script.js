/*1. Об’єктна модель документа потрібна для відображення HTML документу у браузері.
Наприклад кожен HTML-тег є об’єктом, текст всередині тегу також є об’єктом тому така і назва - об’єктна модель документа.*/
/*2. Властивість innerHTMLвстановлює або повертає вміст HTML (внутрішній HTML) елемента.
Властивість innerTextвстановлює або повертає текстовий вміст елемента.
Тобто за допомогою innerHTML ми побачимо і теги якщо вони присутні в елементі. А innerText покаже нам тільки текст в цих внутрішніх тегах якщо вони є.*/
/*3. querySelector, querySelectorAll, getElementById, getElementsByName, getElementsByTagName, getElementsByClassName.
 querySelector, querySelectorAll - найчастіше використовується бо можна використовувати будь-який CSS-селектор. */

 const text = document.querySelectorAll("p");
 text.forEach(element =>{
    element.classList.add("first-task");
 })
 
 const optionsList = document.querySelector("#optionsList");
 console.log(optionsList);

 const parentOptionsList = optionsList.parentElement;
 console.log(parentOptionsList);

 const children = optionsList.childNodes;
 console.log(children);

 children.forEach(element =>{
    console.log(`назва: ${element.nodeName}, тип: ${element.nodeType}`)
 })

 const testParagraph = document.querySelector("#testParagraph");
 testParagraph.innerText = "This is a paragraph";
 console.log(testParagraph);

 const mainHeader = document.querySelector(".main-header").children;
 for(let element of mainHeader){
    console.log(element);
    element.classList.add("nav-item");
 }

 const sectionTitle = document.querySelector(".section-title");
 sectionTitle.classList.remove("section-title");
 console.log(sectionTitle);


